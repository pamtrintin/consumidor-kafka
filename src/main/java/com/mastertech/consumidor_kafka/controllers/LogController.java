package com.mastertech.consumidor_kafka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.consumidor_kafka.models.Log;
import com.mastertech.consumidor_kafka.repositories.LogRepository;


@RestController
public class LogController {

	@Autowired
	LogRepository logRepository;
	
	@RequestMapping(method = RequestMethod.POST, path = "/log")
	public Log criarLog(@RequestBody Log log) {
		return logRepository.save(log);
	}
}
