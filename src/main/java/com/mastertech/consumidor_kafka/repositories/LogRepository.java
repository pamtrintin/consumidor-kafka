package com.mastertech.consumidor_kafka.repositories;

import org.springframework.data.repository.CrudRepository;
import com.mastertech.consumidor_kafka.models.Log;

public interface LogRepository extends CrudRepository<Log, Long> {

}
